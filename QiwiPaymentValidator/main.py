import sqlalchemy as database
from datetime import datetime
import time

import sys

from settings import config

from sqlalchemy.orm import declarative_base, Session

from qiwi import QiwiP2P

Base = declarative_base()


class Product(Base):
    __tablename__ = "product"

    product_id = database.Column(database.Integer(), primary_key=True)
    product_name = database.Column(database.String(15), nullable=False)
    product_price = database.Column(database.Integer(), nullable=False)
    product_type = database.Column(database.String(15), nullable=False)
    product_payload = database.Column(database.String(80), nullable=False)


class UserPurchase(Base):
    __tablename__ = "user_purchase"

    id = database.Column(database.String(50), primary_key=True)
    nickname = database.Column(database.String(50), nullable=False)
    created_at = database.Column(database.DateTime(), nullable=False, default=datetime.now())
    status = database.Column(database.String(15), nullable=False, default="WAITING")
    product_id = database.Column(database.Integer(), database.ForeignKey("product.product_id"), nullable=False)
    result_price = database.Column(database.Integer(), nullable=False)

    def update_status(self, session, new_status):
        print("Updating user purchase (", self.id, self.nickname, ") to status:", new_status)
        if new_status == "REJECTED" or new_status == "EXPIRED":
            session.delete(self)
        else:
            self.status = new_status
            session.add(self)


def main():
    commandline_args = sys.argv

    settings = config['development']
    if len(commandline_args) >= 2:
        settings = config[commandline_args[1]]

    engine = database.create_engine(settings.DATABASE_URI)

    session = Session(bind=engine)
    qiwi_api = QiwiP2P(settings.QIWI_PUBLIC_KEY, settings.QIWI_SECRET_KEY)

    while True:
        try:
            validate_all(session, qiwi_api)
        except Exception as e:
            print("Error occurred, while validation purchases!", e)
            time.sleep(200)
        time.sleep(180)


def validate_all(session, qiwi_api):
    for purchase in session.query(UserPurchase).all():
        if purchase.status == "WAITING":
            status = qiwi_api.get_bill_status(purchase.id)

            if status == purchase.status:
                continue
            purchase.update_status(session, status)

    session.commit()


if __name__ == '__main__':
    main()