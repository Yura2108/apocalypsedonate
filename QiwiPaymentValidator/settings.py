class Config(object):
    DB_HOST = ""
    DB_USER = ""
    DB_PASSWORD = ""
    DB_NAME = "ApocalypseDonate"

    @property
    def DATABASE_URI(self):
        return f"mysql://{self.DB_USER}:{self.DB_PASSWORD}@{self.DB_HOST}/{self.DB_NAME}"


class DevelopmentConfig(Config):
    DB_HOST = "localhost:3306"
    DB_USER = ""
    DB_PASSWORD = ""

    QIWI_PUBLIC_KEY = ""
    QIWI_SECRET_KEY = ""

class TestConfig(Config):
    TESTING = True

    @property
    def DATABASE_URI(self):
        return f"sqlite://"


class ProductionConfig(Config):
    DB_HOST = "localhost:3306"
    DB_USER = "root"
    DB_PASSWORD = "Js0O30E^uEweK67u"

    QIWI_PUBLIC_KEY = ""
    QIWI_SECRET_KEY = ""


config = {
    'development': DevelopmentConfig(),
    'production': ProductionConfig(),
    'testing': TestConfig(),
}
