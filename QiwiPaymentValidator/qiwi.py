import uuid
from datetime import datetime, timedelta
import requests


class QiwiP2P:
    current_timezone = "+03:00"
    datetime_format = f"%Y-%m-%dT%H:%M:%S{current_timezone}"

    bills_api_url = "https://api.qiwi.com/partner/bill/v1/bills/"

    public_key = ""
    secret_key = ""

    def __init__(self, public_key, secret_key):
        self.public_key = public_key
        self.secret_key = secret_key

    @staticmethod
    def _format_qiwi_dict(dictionary):
        return str(dictionary).replace("'", "\"")

    def get_bill_status(self, bill_id) -> str:
        response = requests.get(
            url=self.bills_api_url + bill_id,
            headers=self._get_default_headers()
        )

        return response.json().get('status')['value']

    def _format_time(self, time: datetime):
        return time.strftime(self.datetime_format)

    def _get_default_headers(self):
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'authorization': 'Bearer ' + self.secret_key
        }
