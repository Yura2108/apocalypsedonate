from ApocalypseDonate import create_app
from ApocalypseDonate.extensions import database
import pytest


@pytest.fixture(scope="module")
def app():
    """sdf"""
    application = create_app("testing")

    # setup database
    with application.app_context():
        database.create_all()

    yield application


@pytest.fixture(scope="module")
def client(app):
    """fds"""
    return app.test_client()
