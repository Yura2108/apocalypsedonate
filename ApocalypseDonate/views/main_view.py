from flask_classful import FlaskView, route
from flask import render_template

from ApocalypseDonate.models import Product, MediaInformation


class MainView(FlaskView):
    @route("/")
    def index(self):
        return render_template(
            "index.html", **MediaInformation.get_all_media()
        )

    @route("/donate")
    def donate(self):
        return render_template(
            "donate.html", **MediaInformation.get_all_media(), products=Product.get_all_products()
        )

    @route("/start")
    def start(self):
        return render_template(
            "start.html", **MediaInformation.get_all_media()
        )

    @route("/rules")
    def rules(self):
        return render_template(
            "rules.html", **MediaInformation.get_all_media()
        )
