from flask_classful import FlaskView, route
from flask import request, abort, redirect, current_app

from ApocalypseDonate.models import Product, PromoCode, UserPurchase


from ApocalypseDonate.services import payments

from functools import wraps
import json


def token_required(function):
    @wraps(function)
    def wrap(*args, **kwargs):
        authorization_data = request.headers.get('Authorization', None)

        if authorization_data is None:
            return {
                       "message": "Authentication Token is missing!",
                       "data": None,
                       "error": "Unauthorized"
                   }, 401

        try:
            token = authorization_data.split()[1]

            if token != current_app.config.get('API_TOKEN'):
                return {
                           "message": "Authentication Token is invalid!",
                           "data": None,
                           "error": "Unauthorized"
                       }, 401

            return function(*args, *kwargs)
        except IndexError:
            return {
                       "message": "Authentication Token is invalid!",
                       "data": None,
                       "error": "Unauthorized"
                   }, 401

    return wrap


class ApiView(FlaskView):
    @route("payment")
    def payment(self):
        request_vars = request.args.to_dict()

        if len(request_vars) < 2 or ('productId' not in request_vars.keys()) or ('nickName' not in request_vars.keys()):
            abort(400)

        product_id = request_vars.get("productId")
        nickname = request_vars.get("nickName")

        promo_code = ''
        if 'promoCode' in request_vars.keys():
            promo_code = request_vars.get('promoCode')

        if not product_id.isdigit():
            abort(400)

        product_id = int(product_id)
        product_model: Product = Product.get_product_by_id(product_id)

        if product_model is None:
            abort(404)

        total_price = PromoCode.try_apply_promo_code(promo_code, product_model.product_price)

        redirect_url = payments.create_bill(nickname, product_id, total_price)

        return redirect(redirect_url)

    #
    # @route("add")
    # def add_product(self):
    #     products = cache.get("Products")
    #
    #     if not isinstance(products, list):
    #         return "API ERROR"
    #
    #     product = Product(product_name="test", product_price=9999)
    #
    #     print(f"Created: {product}")
    #
    #     database.session.add(product)
    #     database.session.commit()
    #
    #     print(f"Added {product}")
    #
    #     products.append(product)
    #     cache.set("Products", products)
    #
    #     return ""

    @route("purchase/get-waiting-users")
    @token_required
    def get_waiting_users(self):
        filtered_purchases = filter(lambda purchase: purchase.status == "PAID", UserPurchase.get_all_purchases())

        response = []

        for purchase in filtered_purchases:
            product: Product = Product.get_product_by_id(purchase.product_id)

            response.append({
                "id": purchase.id,
                "nickname": purchase.nickname,
                "product_type": product.product_type,
                "product_payload": product.product_payload.replace("{username}", purchase.nickname)
            })

        return json.dumps(response)

    @route("purchase/make-issued", methods=['POST'])
    @token_required
    def make_issued(self):
        data = request.json

        if len(request.json) == 0:
            return {
                       "message": "Invalid request form data",
                       "data": None,
                       "error": "Invalid request form data"
            }, 400

        if not isinstance(data, list) or not isinstance(data[0], str):
            return {
                       "message": "Request data must be array of IDs",
                       "data": None,
                       "error": "Invalid request form data"
            }, 400

        UserPurchase.make_issued(data)
        return "Successful!"
