from flask import Flask
import click

from .settings import config
from .views import MainView, ApiView
from .extensions import database, cache, qiwi

from .models import Product

from .services import payments


def create_app(config_name='development'):
    application = Flask(
        "ApocalypseDonate",
        template_folder="./templates",
        static_url_path="/assets",
        static_folder="./assets"
    )

    application.config.from_object(config[config_name])

    application.config['SQLALCHEMY_DATABASE_URI'] = application.config.get('DATABASE_URI')
    application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    register_views(application)
    register_extensions(application)
    register_commands(application)

    return application


def register_views(application):
    MainView.register(application, route_base="/")
    ApiView.register(application, route_base="/api")


def register_extensions(application):
    database.init_app(application)
    cache.init_app(application)
    qiwi.init_app(application)


def register_commands(application):
    @application.cli.command()
    @click.option('--drop', is_flag=True, help='Create after drop.')
    def initdb(drop):
        """Initialize the database."""
        if drop:
            click.confirm('This operation will delete the database, do you want to continue?', abort=True)
            database.drop_all()
            click.echo('Drop tables.')
        database.create_all()
        click.echo('Initialized database.')

