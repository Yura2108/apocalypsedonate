from .extensions import database, cache
import sqlalchemy

from datetime import datetime


class Product(database.Model):
    product_id = sqlalchemy.Column(sqlalchemy.Integer(), primary_key=True)
    product_name = sqlalchemy.Column(sqlalchemy.String(15), nullable=False)
    product_price = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False)
    product_type = sqlalchemy.Column(sqlalchemy.String(15), nullable=False)
    product_payload = sqlalchemy.Column(sqlalchemy.String(80), nullable=False)

    @staticmethod
    @cache.cached(timeout=0, key_prefix="Products")
    def get_all_products() -> list:
        return Product.query.all()

    @staticmethod
    @cache.memoize(timeout=50)
    def get_product_by_id(product_id):
        products_list = Product.get_all_products()

        for product in products_list:
            if product.product_id == product_id:
                return product

        return None

    def __str__(self):
        return f"{self.product_id} - {self.product_name} - {self.product_price}"


class PromoCode(database.Model):
    promo_code_id = sqlalchemy.Column(sqlalchemy.Integer(), primary_key=True)
    promo_code_string = sqlalchemy.Column(sqlalchemy.String(15), nullable=False)
    discount = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False)
    expire_on = sqlalchemy.Column(sqlalchemy.DateTime(), nullable=False)

    @staticmethod
    @cache.cached(timeout=0, key_prefix="PromoCodes")
    def get_all_promo_codes() -> list:
        return PromoCode.query.all()

    @staticmethod
    @cache.memoize(timeout=50)
    def try_apply_promo_code(string, base_price):
        promo_codes: list[PromoCode] = PromoCode.get_all_promo_codes()

        for promo_code in promo_codes:
            if promo_code.promo_code_string == string:
                return promo_code.get_total_price(base_price)

        return base_price

    def get_total_price(self, price):
        return price - (price * self.discount)

    def __str__(self):
        return f"{self.promo_code_id} | {self.promo_code_string} | {self.discount}% | {self.expire_on}"


class UserPurchase(database.Model):
    """
    Модель покупки пользователя. Будет использоваться для выдачи товара, отслеживания статуса заказа
    """

    id = sqlalchemy.Column(sqlalchemy.String(50), primary_key=True)
    nickname = sqlalchemy.Column(sqlalchemy.String(50), nullable=False)
    created_at = sqlalchemy.Column(sqlalchemy.DateTime(), nullable=False, default=datetime.now())
    status = sqlalchemy.Column(sqlalchemy.String(15), nullable=False, default="WAITING")
    product_id = sqlalchemy.Column(sqlalchemy.Integer(), sqlalchemy.ForeignKey(Product.product_id), nullable=False)
    result_price = sqlalchemy.Column(sqlalchemy.Integer(), nullable=False)

    @staticmethod
    # TODO need to be cached?
    # @cache.cached(key_prefix="UserPurchases")
    def get_all_purchases() -> list:
        return UserPurchase.query.all()

    @staticmethod
    @cache.cached(timeout=50)
    def get_purchase_by_id(purchase_id):
        for purchase in UserPurchase.get_all_purchases():
            if purchase.id == purchase_id:
                return purchase

        return None

    @staticmethod
    def create_new(purchase_id, nickname, product_id, price):
        model = UserPurchase(id=purchase_id, nickname=nickname, product_id=product_id, result_price=price)

        database.session.add(model)
        database.session.commit()

    @staticmethod
    def make_issued(purchases: list[str]):
        for purchase in UserPurchase.get_all_purchases():
            if purchase.id not in purchases or purchase.status != "PAID":
                continue

            purchase.status = "ISSUED"
            database.session.commit()

    def __str__(self):
        return f"{self.id} | {self.nickname} | {self.result_price} | {self.status}"


class MediaInformation(database.Model):
    """
    Модель для хранения медиа-информации: Ссылки на соц. сети, IP сервера
    """

    id = sqlalchemy.Column(sqlalchemy.Integer(), primary_key=True)
    title = sqlalchemy.Column(sqlalchemy.String(10), nullable=False)
    content = sqlalchemy.Column(sqlalchemy.String(50), nullable=False)

    @staticmethod
    @cache.cached(timeout=0, key_prefix="MediaInformation")
    def get_all_media() -> dict[str, str]:
        result_dict = {}

        for media in MediaInformation.query.all():
            result_dict.update({media.title: media.content})

        return result_dict

