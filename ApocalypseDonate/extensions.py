from flask_sqlalchemy import SQLAlchemy
from flask_caching import Cache

from .payments import QiwiP2P

database: SQLAlchemy = SQLAlchemy()
cache: Cache = Cache()

qiwi = QiwiP2P()
