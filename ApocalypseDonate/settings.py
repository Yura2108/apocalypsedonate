class Config(object):
    DEBUG = False
    DEVELOPMENT = False

    DB_HOST = ""
    DB_USER = ""
    DB_PASSWORD = ""
    DB_NAME = "ApocalypseDonate"

    TABLE_NAME = "user_purchase"

    API_TOKEN = ""

    @property
    def DATABASE_URI(self):
        return f"mysql://{self.DB_USER}:{self.DB_PASSWORD}@{self.DB_HOST}/{self.DB_NAME}"


class DevelopmentConfig(Config):
    DB_HOST = "localhost:3306"
    DB_USER = ""
    DB_PASSWORD = ""

    QIWI_PUBLIC_KEY = ""
    QIWI_SECRET_KEY = ""

    API_TOKEN = ""

    # maybe redis?
    CACHE_TYPE = "SimpleCache"


class TestConfig(Config):
    TESTING = True

    CACHE_TYPE = "SimpleCache"

    @property
    def DATABASE_URI(self):
        return f"sqlite://"


config = {
    'development': DevelopmentConfig(),
    'production': None,
    'testing': TestConfig(),
}
