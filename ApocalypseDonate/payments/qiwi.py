import uuid
from datetime import datetime, timedelta
import requests


class QiwiP2P:
    current_timezone = "+03:00"
    datetime_format = f"%Y-%m-%dT%H:%M:%S{current_timezone}"

    bills_api_url = "https://api.qiwi.com/partner/bill/v1/bills/"

    public_key = ""
    secret_key = ""

    def init_app(self, application):
        config = application.config
        if "QIWI_PUBLIC_KEY" in config:
            self.public_key = config['QIWI_PUBLIC_KEY']

        if "QIWI_SECRET_KEY" in config:
            self.secret_key = config['QIWI_SECRET_KEY']

    @staticmethod
    def _format_qiwi_dict(dictionary):
        return str(dictionary).replace("'", "\"")

    def create_bill(self, amount, comment="", lifetime_hours=2):
        params = {
            "amount": {
                "currency": "RUB",
                "value": amount
            },
            "comment": comment,
            "expirationDateTime": self._format_time(datetime.now() + timedelta(hours=lifetime_hours))
        }

        bill_id = str(uuid.uuid4())

        response = requests.put(
            url=self.bills_api_url + bill_id,
            headers=self._get_default_headers(),
            data=self._format_qiwi_dict(params)
        )

        if not response.ok:
            # TODO Exception
            pass

        return bill_id, response.json().get('payUrl')

    def _format_time(self, time: datetime):
        return time.strftime(self.datetime_format)

    def _get_default_headers(self):
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'authorization': 'Bearer ' + self.secret_key
        }