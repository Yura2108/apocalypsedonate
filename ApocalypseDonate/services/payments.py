from ApocalypseDonate.extensions import qiwi
from ApocalypseDonate.models import UserPurchase


def create_bill(nickname, product_id, price, comment="") -> str:
    """
    Creates a new bill
    :param nickname: User Nickname
    :param product_id Product ID in DataBase
    :param price: result price (with applying promo codes, etc.)
    :param comment: optional payment comment
    :return: Payment url (For redirecting)
    """

    created_bill = qiwi.create_bill(price, comment)

    bill_id = created_bill[0]

    UserPurchase.create_new(bill_id, nickname, product_id, price)

    return created_bill[1]
