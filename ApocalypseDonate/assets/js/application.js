const popupDiv = $('div.popup')[0];

const popupTitleElement = $('.popup__title')[0];
const popupPriceElement = $('.price')[0];
const popupAgreementElement = $('input.agreement')[0];

const popupNicknameInput = $('input.nickname')[0];
const popupPromoCodeInput = $('input.promocode')[0];

const popupBuyButton = $('.popup__buy__button');

const navigationBar = $('#nav');

$('.popup-link').on('click',
    function (event) {
        let id = event.currentTarget.id;

        popupDiv.style.display = 'block';
        popupDiv.style.opacity = '1';

        let collection = event.currentTarget.parentElement.children;

        let price, title;

        for (const index in collection) {
            let element = collection[index];

            if (typeof element !== "object") {
                continue;
            }

            if(element.classList.contains("card__price")) {
                price = element.textContent;
            }else if(element.classList.contains("card__name")) {
                title = element.textContent;
            }
        }

        updatePopUpData(title, price, id);
    }
);

$('#navToggle').on('click',
    function (event) {
        event.preventDefault();

        navigationBar.toggleClass("show");
});

$('.close-popup').on('click', hideCurrentPopUp);

$('.popup__area').on('click', hideCurrentPopUp);

function hideCurrentPopUp() {
    popupDiv.style.display = 'none';
    popupDiv.style.opacity = '0';

    updatePopUpData('', '', -1);
}

function updatePopUpData(title, price, product_id) {
    popupTitleElement.textContent = title;
    popupPriceElement.textContent = price;
    popupBuyButton.id = product_id;
}

popupBuyButton.on(
    'click', function () {
        let isAgreementChecked = popupAgreementElement.checked;

        if(isAgreementChecked === false) {
            return;
        }

        let productId = popupBuyButton.id;
        let nickName = popupNicknameInput.value;
        let promoCode = popupPromoCodeInput.value;

        if(productId <= 0 || nickName === "" || nickName.includes(" ")) {
            return;
        }

        window.location
            .replace("/api/payment?productId=" + productId + "&nickName=" + nickName + "&promoCode=" + promoCode)
    }
);

function copyTextToClipboard(copyText) {
    navigator.clipboard.writeText(copyText);
}